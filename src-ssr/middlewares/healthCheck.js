import { ssrMiddleware } from "quasar/wrappers";

//Health Check middleware for cubernetes request
export default ssrMiddleware(({ app, resolve }) => {
  app.get(resolve.urlPath("healthcheck"), async (req, res) => {
    //?
    res.setHeader("Content-Type", "application/json");
    res.json({
      status: 'UP',
    });
  });
});
