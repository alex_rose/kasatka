import { ssrMiddleware } from "quasar/wrappers";
import axios from "axios";

const defaultMeta = {
  title: "KASATKA",
  description: "KASATKA",
  keywords: "KASATKA",
  ogTitle: "KASATKA",
  ogType: "KASATKA",
  ogUrl: "KASATKA",
  ogDescription: "KASATKA",
  ogImage: "KASATKA",
};

// This middleware should execute as last one
// since it captures everything and tries to
// render the page with Vue

const api = axios.create({ baseURL: process.env.VUE_APP_BASE_URL });

export default ssrMiddleware(
  ({ app, resolve, publicPath, folders, render, serve }) => {
    //main
    app.get(resolve.urlPath("/healthcheck"), async (req, res) => {
      res.status(200).json({ status: "OK"})
    })
    app.get(resolve.urlPath("/"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });

    //projects
    app.get(resolve.urlPath("/projects"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });

    //projects/:id
    app.get("/projects/:id", async (req, res) => {
      const id = req.params.id;
      if (id.length === 36) {
        const meta = await api({
          method: "get",
          url: `/meta/${req.params.id}`,
        })
          .then((response) => {
            return response.data;
          })
          .catch((e) => {
            return defaultMeta;
          });

        const title = meta.title;
        const description = meta.description;
        const keywords = meta.keywords;
        const ogTitle = meta.ogTitle;
        const ogType = meta.ogType;
        const ogUrl = meta.ogUrl;
        const ogDescription = meta.ogDescription;
        const ogImage = meta.ogImage;
        render({
          req,
          res,
          title,
          keywords,
          description,
          ogTitle,
          ogType,
          ogUrl,
          ogDescription,
          ogImage,
        }).then((html) => {
          res.send(html);
        });
      }
    });

    //contacts
    app.get(resolve.urlPath("/contacts"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });

    //about
    app.get(resolve.urlPath("/about"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });

    //feedback
    app.get(resolve.urlPath("/feedback"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });

    //studio
    app.get(resolve.urlPath("/studio"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });

    //blog
    app.get(resolve.urlPath("/blog"), async (req, res) => {
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: req.url },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });

      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      }).then((html) => {
        res.send(html);
      });
    });
  }
);
