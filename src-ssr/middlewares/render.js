import { ssrMiddleware } from "quasar/wrappers";
//import fetchMetaAPI from "boot/api/meta.js";
import axios from "axios";

// This middleware should execute as last one
// since it captures everything and tries to
// render the page with Vue

const defaultMeta = {
  title: "KASATKA",
  description: "KASATKA",
  keywords: "KASATKA",
  ogTitle: "KASATKA",
  ogType: "KASATKA",
  ogUrl: "KASATKA",
  ogDescription: "KASATKA",
  ogImage: "KASATKA",
};

const api = axios.create({ baseURL: process.env.VUE_APP_BASE_URL });

export default ssrMiddleware(
  ({ app, resolve, publicPath, folders, render, serve }) => {
    // we capture any other Express route and hand it
    // over to Vue and Vue Router to render our page

    //!!!Other pages and not founded pages, meta inherites by main page "/"!!!
    app.get(resolve.urlPath("*"), async (req, res) => {
      res.setHeader("Content-Type", "text/html");
      const meta = await api({
        method: "post",
        url: "/meta",
        data: { path: "/" },
      })
        .then((response) => {
          return response.data;
        })
        .catch((e) => {
          return defaultMeta;
        });
      const title = meta.title;
      const description = meta.description;
      const keywords = meta.keywords;
      const ogTitle = meta.ogTitle;
      const ogType = meta.ogType;
      const ogUrl = meta.ogUrl;
      const ogDescription = meta.ogDescription;
      const ogImage = meta.ogImage;
      render({
        req,
        res,
        title,
        keywords,
        description,
        ogTitle,
        ogType,
        ogUrl,
        ogDescription,
        ogImage,
      })
        .then((html) => {
          res.send(html);
        })
        .catch((err) => {
          if (err.url) {
            if (err.code) {
              res.redirect(err.code, err.url);
            } else {
              res.redirect(err.url);
            }
          } else if (err.code === 404) {
            // hmm, Vue Router could not find the requested route

            // Should reach here only if no "catch-all" route
            // is defined in /src/routes
            res.status(404).send("404 | Page Not Found");
          } else if (process.env.DEV) {
            // well, we treat any other code as error;
            // if we're in dev mode, then we can use Quasar CLI
            // to display a nice error page that contains the stack
            // and other useful information

            // serve.error is available on dev only
            serve.error({ err, req, res });
          } else {
            // we're in production, so we should have another method
            // to display something to the client when we encounter an error
            // (for security reasons, it's not ok to display the same wealth
            // of information as we do in development)

            // Render Error Page on production or
            // create a route (/src/routes) for an error page and redirect to it
            res.status(500).send("500 | Internal Server Error");
          }
        });
    });
  }
);
