const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        meta: {
          isBreadCrumbs: false,
          title: 'Главная'
        },
        component: () => import("pages/Index.vue"),
      },
      {
        path: "/projects",
        name: "ProjectsPage",
        meta: {
          isBreadCrumbs: true,
          title: 'Проекты',
          crumbs: [{label: 'Главная', to: '/'}, {label: 'Проекты', to: '/projects'}]
        },
        component: () => import("pages/ProjectsPage"),
      },
      {
        path: "projects/:projectId",
        name: "ProjectPage",
        meta: {
          isBreadCrumbs: true,
          title: 'Проект',
          crumbs: [{label: 'Главная', to: '/'}, {label: 'Проекты', to: '/projects'}, {label: 'Проект', to: ''}]
        },
        component: () => import("pages/ProjectPage"),
      },
      {
        path: "about",
        name: "AboutPage",
        meta: {
          isBreadCrumbs: true,
          title: 'О нас'
        },
        component: () => import("pages/AboutPage"),
      },
      {
        path: "contacts",
        name: "ContactsPage",
        meta: {
          isBreadCrumbs: true,
          title: 'Контакты'
        },
        component: () => import("pages/ContactsPage"),
      },
      {
        name: "service",
        path: "services",
        component: () => import("pages/Services"),
        meta: {
          isBreadCrumbs: true,
          title: "Услуги",
        },
      },
    ],
  },
  {
    path: "/auth",
    component: () => import("layouts/AuthLayout"),
    children: [
      {
        path: "",
        component: () => import("pages/cms/auth/AuthPage"),
        redirect: { name: "signin" },
      },
      {
        name: "signin",
        path: "signin",
        component: () => import("pages/cms/auth/SignIn"),
      },
    ],
  },
  {
    path: "/admin",
    name: "admin",
    component: () => import("layouts/AdminLayout.vue"),
    children: [
      {
        name: "panel",
        path: "",
        component: () => import("pages/cms/ControlPanel.vue"),
        meta: {
          title: "Главная",
          icon: "dashboard",
          isAuthRequired: true,
        },
      },
      {
        name: "settings",
        path: "settings",
        component: () => import("pages/cms/Settings.vue"),
        meta: {
          title: "Настройки",
          icon: "settings",
          isAuthRequired: true,
          isShown: true
        },
      },
      {
        name: "blogs",
        path: "blogs",
        component: () => import("src/pages/cms/blog/Blogs.vue"),
        meta: {
          title: "Блог",
          icon: "dynamic_feed",
          isAuthRequired: true,
          isShown: false
        },
        children: [
          {
            path: "blogs/:blogId",
            component: () => import("src/pages/cms/project/EditProject.vue"),
            meta: {
              isAuthRequired: true,
            },
          },
        ],
      },
      {
        name: "projects",
        path: "projects",
        redirect: { name: "TableProjects" },
        component: () => import("src/pages/cms/project/Projects.vue"),
        children: [
          {
            name: "TableProjects",
            path: "table",
            component: () => import("src/pages/cms/project/TableProjects.vue"),
            meta: {
              isAuthRequired: true,
            },
          },
          {
            name: "CreateProject",
            path: "createproject",
            component: () => import("src/pages/cms/project/CreateProject.vue"),
            meta: {
              isAuthRequired: true,
              title: "Добавить проект",
            },
          },
          {
            name: "EditProject",
            path: ":projectId",
            component: () => import("src/pages/cms/project/EditProject.vue"),
            meta: {
              isAuthRequired: true,
              title: "Редактировать проект",
            },
          },
        ],
        meta: {
          title: "Проекты",
          icon: "ballot",
          isAuthRequired: true,
          isShown: true
        },
      },
      {
        name: "services",
        path: "services",
        component: () => import("pages/cms/services/"),
        meta: {
          isAuthRequired: true,
          title: "Услуги",
          icon: "design_services",
          isShown: true
        },
        children: [
          {
            name: "serviceList",
            path: "",
            component: () => import("pages/cms/services/ServicesList"),
            meta: {
              isAuthRequired: true,
              isShown: false
            },
          },
          {
            name: "editService",
            path: ":id",
            component: () => import("pages/cms/services/EditService"),
            meta: {
              isAuthRequired: true,
              title: "Редактировать услугу",
              isShown: false
            },
          },
          {
            name: "addService",
            path: "create-service",
            component: () => import("pages/cms/services/EditService"),
            meta: {
              isAuthRequired: true,
              title: "Добавить услугу",
              isShow: false
            },
          },
        ],
      },
      {
        name: "orders",
        path: "orders",
        component: () => import("src/pages/cms/order/TableOrders.vue"),
        meta: {
          title: "Заявки",
          icon: "contact_phone",
          isShown: true
        },
      },
      {
        name: "settings",
        path: "settings",
        component: () => import("pages/cms/Settings.vue"),
        meta: {
          title: "Настройки",
          icon: "settings",
          isAuthRequired: true,
          isShown: true
        },
      },
      {
        name: "meta",
        path: "meta",
        component: () => import("pages/cms/meta/MainMeta.vue"),
        meta: {
          title: "SEO страниц",
          icon: "html",
          isAuthRequired: true,
          isShown: true
        },
      },
    ],
    meta: {
      isAuthRequired: true,
    },
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
