import VueSanitize from "vue-sanitize";

export default boot(({ app }) => {
  app.use(VueSanitize);
  app.config.globalProperties.$sanitize = VueSanitize;
});
