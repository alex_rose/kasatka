import { api } from "../axios";

export async function newOrderAPI(orderData) {
  try {
    return api({
      method: "POST",
      url: "/order",
      data: orderData,
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function deleteOrderAPI(id) {
  try {
    return api({
      method: "DELETE",
      url: `/order/${id}`,
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function getOrdersAPI() {
  try {
    return api({
      method: "GET",
      url: "/order",
    }).then((res) => {
      return res.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}
