import { api } from "../axios";
const SERVICE_API_URL = 'service'

export const getServices = async () => {
  try {
    const {data} = await api({
      method: 'GET',
      url: SERVICE_API_URL
    })

    return data
  } catch (e) {
    console.error(e)
  }
}
export const getServiceById = async (id) => {
  try {
    const {data} = await api({
      method: 'GET',
      url: `${SERVICE_API_URL}/${id}`
    })

    return data
  } catch (e) {
    console.error(e)
  }
}
export const putService = async (service) => {
  try {
    return api({
      method: 'PUT',
      url: `${SERVICE_API_URL}/${service.id}`,
      data: service
    })
  } catch (e) {
    console.error(e)
  }
}
export const deleteService = async (id) => {
  try {
    return api({
      method: 'DELETE',
      url: `${SERVICE_API_URL}/${id}`,
    })
  } catch (e) {
    console.error(e)
  }
}
export const postService = async (service) => {
  try {
    return api({
      method: 'POST',
      url: SERVICE_API_URL,
      data: service
    })
  } catch (e) {
    console.error(e)
  }
}
