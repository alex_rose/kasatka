import { api } from '../axios.js';

export async function uploadFile(file) {
  const form = new FormData();
  form.append('file', file);

  try {
    const file = await api({
      method: 'POST',
      url: 'file/upload',
      onUploadProgress: progress => {
        const { total, loaded } = progress;
        let totalSizeInMB = total;
        let loadedSizeInMB = loaded;
      },
      data: form,
    })
  } catch (e) {

  }
}

export async function deleteFile(key) {
  try {
    const res = await api({
      method: 'DELETE',
      url: `file/${key}`
    })
    return res
  } catch (error) {
    
  }
}