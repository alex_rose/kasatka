import { api } from "boot/axios.js";

export async function fetchAboutAPI() {
  try {
    return api({
      method: "GET",
      url: "/text/about",
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function putAboutAPI(data) {
  try {
    return api({
      method: "PUT",
      url: "/text/about",
      data: data,
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function getContactsAPI() {
  try {
    return api({
      method: "GET",
      url: "/text/contacts",
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function putContactsAPI(data) {
  try {
    return api({
      method: "PUT",
      url: "/text/contacts",
      data: data,
    });
  } catch (e) {
    throw new Error(e);
  }
}
