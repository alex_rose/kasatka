import {api} from "boot/axios";

export async function sendVideoAPI(fileData) {
  const accessToken = localStorage.getItem("accessToken")
  try {
    return api({
      method: "POST",
      url: "/main_videos/upload",
      data: fileData,
      headers: {
        "Content-Type": "multipart/form-data",
        "Authorization": `Basic ${accessToken}`,
      },
    }).then((response) => {
      console.log(response, 'axios')
      return response.data
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function fetchVideosAPI() {
  try {
    return api({
      method: "GET",
      url: '/main_videos/',
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function deleteVideoAPI(id) {
  try {
    return api({
      method: "DELETE",
      url: `/main_videos/${id}`,
    });
  } catch (e) {
    throw new Error(e);
  }
}
