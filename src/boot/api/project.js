import { api } from "../axios";

export async function fetchProjectAPI(projectId) {
  try {
    return api({
      method: "GET",
      url: `/projects/${projectId}`,
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function createProjectAPI(projectFormData) {
  const accessToken = localStorage.getItem("accessToken")
  if(!accessToken) return false
  try {
    return api({
      method: "POST",
      url: "/projects/createProject",
      data: projectFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        "Authorization": `Basic ${accessToken}`,
      },
    }).then((response) => {return response});
  } catch (e) {
    throw new Error(e);
  }
}

export async function deleteProjectAPI(id) {
  try {
    return api({
      method: "DELETE",
      url: `/projects/${id}`,
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function putProjectAPI(projectFormData, projectId) {

  try {
    return api({
      method: "PUT",
      url: `/projects/${projectId}`,
      data: projectFormData,
    });
  } catch (e) {
    throw new Error(e);
  }
}
