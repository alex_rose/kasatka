import { boot } from "quasar/wrappers";
import { api } from "../axios";

export async function fetchMetaAPI() {
  try {
    return api({
      method: "GET",
      url: "/meta",
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function putMetaAPI(meta) {
  try {
    return api({
      method: "PUT",
      url: "/meta",
      data: meta,
    });
  } catch (e) {
    throw new Error(e);
  }
}
