import { api } from "boot/axios.js";

export const signInAPI = async (user) => {
  try {
    return api({
      method: "POST",
      url: "/user/signin",
      data: user,
    }).then((response)=> {
      if(!response) return false
      return response.data
    });
  } catch (e) {
    throw new Error(e);
  }
}

export const signUpAPI = async (user) => {
  try {
    return api({
      method: "POST",
      url: "/user/signup",
      data: user,
    });
  } catch (e) {
    throw new Error(e);
  }
}
