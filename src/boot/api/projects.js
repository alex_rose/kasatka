import { api } from "../axios";
export async function fetchProjectsAPI() {
  try {
    return api({
      method: "GET",
      url: "/projects/",
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}

export async function fetchRandomProjectsAPI() {
  try {
    return api({
      method: "GET",
      url: "/projects/random",
    }).then((response) => {
      return response.data;
    });
  } catch (e) {
    throw new Error(e);
  }
}
