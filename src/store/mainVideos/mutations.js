/*
export function someMutation (state) {
}
*/

export function addVideos(state, videos) {
  state.mainVideos = videos.data
}

export function addVideo(state, video) {
  state.mainVideos.push(video)
}

export function deleteVideo(state, videoId) {
  state.mainVideos = state.mainVideos.filter(video => video.id !== videoId)
}
