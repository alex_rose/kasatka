import {sendVideoAPI, fetchVideosAPI, deleteVideoAPI} from 'boot/api/mainVideos'

export const sendVideo = async ({commit}, video) => {
  const formData = new FormData()
  await formData.append('file', video)
  const sendingVideo = await sendVideoAPI(formData)
  console.log(sendingVideo)
  return await commit('addVideo', sendingVideo.data)
}

export const getVideos = async ({commit}) => {
  const videos = await fetchVideosAPI()
  await commit('addVideos', videos);
}

export const deleteVideo = async({commit}, videoId) => {
  await deleteVideoAPI(videoId)
  await commit('deleteVideo', videoId)
  console.log(videoId)
}
