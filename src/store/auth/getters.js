export function isAuthenticated(state) {
  return state.accessToken
}

export function userFullName(state) {
  return `${state.firstName} ${state.lastName}`
}