export default function () {
  return {
    accessToken: '',
    refreshToken: '',
    user: {
      firstName: '',
      lastName: ''
    }
  }
}
