export function SET_TOKENS(state, payload) {
  Object.entries(payload).forEach(([key, value]) => {
    state[key] = value;
  })
}

export function SET_USER(state, payload) {
  state.user = payload
}