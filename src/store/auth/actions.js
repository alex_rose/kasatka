import { signUpAPI, signInAPI } from "boot/api/user.js";
import {LocalStorage, Notify} from "quasar";
import * as jose from 'jose'

export async function signInUser({ commit }, user) {
  const { tokens } = await signInAPI(user);

  if (!tokens) {
    Notify.create({message: 'Ошибка авторизации', type: 'negative'})
    return false
  }

  LocalStorage.set("accessToken", tokens.accessToken);
  LocalStorage.set("refreshToken", tokens.refreshToken);
  commit('SET_TOKENS', tokens)
  const userProfile = jose.decodeJwt(tokens.accessToken)
  commit('SET_USER', userProfile)
  Notify.create({ message: 'Успешная авторизация', type: 'positive' })
  return tokens
}

export async function signUpUser(ctx, user) {
  await signUpAPI(user);
}
