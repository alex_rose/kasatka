import { fetchBlogsAPI } from "../../boot/api/blogs.js";
export async function fetchBlogs({ commit }) {
  const blogs = await fetchBlogsAPI();
  commit("setBlogs", blogs);
  return;
}
export async function fetchBlog({ commit }, blogId) {
  //return await ;
}
export async function editBlog({ commit }, blogId) {
  //return await ;
}

export async function updateBlog({ commit }, blogId) {
  //return await ;
}
