/*
export function someGetter (state) {
}
*/
export const getColumns = (state) => {
  return [
    {
      name: "name",
      required: true,
      label: "Название Блога",
      align: "left",
      field: (row) => row.name,
      format: (val) => `${val}`,
      sortable: true,
    },
    {
      name: "id",
      align: "center",
      label: "id",
      field: "id",
      sortable: true,
    },
    { name: "title", label: "title", field: "title", sortable: true },
    { name: "created", label: "created", field: "created" },
    { name: "status", label: "status", field: "status" },
    { name: "action", label: "action", field: "action" },
  ]
}
