import { newOrderAPI, deleteOrderAPI, getOrdersAPI } from "boot/api/order.js";

export async function newOrder(ctx, orderData) {
  await newOrderAPI(orderData);
  return;
}

export async function deleteOrder(ctx, orderId) {
  await deleteOrderAPI(orderId);
  return;
}

export async function getOrders() {
  return await getOrdersAPI();
}
