import { fetchMetaAPI, putMetaAPI } from "../../boot/api/pages.js";
export async function fetchMeta({ commit }) {
  const meta = await fetchMetaAPI();
  commit("setMeta", meta);
}

export async function updateMeta({ commit }, meta) {
  return await putMetaAPI(meta);
}
