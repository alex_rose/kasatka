import { store } from "quasar/wrappers";
import { createStore } from "vuex";
import BlogsModule from "./blogs/index.js";
import ProjectsModule from "./projects/index.js";
import AuthModule from "./auth/index.js";
import PagesModule from "./pages/index.js";
import OrderModule from "./order/index.js";
import GeneralModule from "./general/index.js";
import TextModule from "./texts/index.js";
import ServicesModule from "./services/";
import MainVideos from "./mainVideos/index.js";
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/*{ ssrContext }*/) {
  const Store = createStore({
    modules: {
      pages: PagesModule,
      blogs: BlogsModule,
      projects: ProjectsModule,
      auth: AuthModule,
      order: OrderModule,
      general: GeneralModule,
      texts: TextModule,
      services: ServicesModule,
      mainVideos: MainVideos
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  });

  return Store;
});
