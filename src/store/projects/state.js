export default function () {
  return {
    projects: [],
    selectedProject: {
      id: 1,
      description: "description",
      finishDate: "12.12.12",
      created_at: "11.11.11",
      location: "Москва",
      media: [],
      meta: {},
      square: "111m2",
      title: "Дом",
    },
    randomizedProjects: [],
  };
}
