import {
  fetchProjectsAPI,
  fetchRandomProjectsAPI,
} from "boot/api/projects";
import { Notify } from "quasar";
import {
  fetchProjectAPI,
  createProjectAPI,
  deleteProjectAPI,
  putProjectAPI,
} from "boot/api/project";
import { deleteFile } from 'src/boot/api/file';

export async function fetchRandomProjects({ commit }) {
  const projects = await fetchRandomProjectsAPI();
  commit("setRandomProjects", projects);
  return projects;
}

export async function fetchProjects({ commit }) {
  const projects = await fetchProjectsAPI();
  commit("setProjects", projects);
  return projects;
}

export async function fetchProject({ commit }, projectId) {
  const project = await fetchProjectAPI(projectId);
  await commit("selectProject", project);
  return project;
}
export async function fetchProjectWithoutStore({ commit }, projectId) {
  return await fetchProjectAPI(projectId);
}

export async function deleteProject({ commit }, projectId) {
  return await deleteProjectAPI(projectId);
}

export async function createProject({ commit }, projectFormData) {
  const response = await createProjectAPI(projectFormData);
  if (response.status === 201) {
    await this.$router.push({name: "TableProjects"});
    Notify.create("Проект создан");
  }
}

export async function updateProject({ commit, state }, [media, modeling] = [[], []]) {
  const projectToUpdate = JSON.parse(JSON.stringify(state.selectedProject));
  projectToUpdate.media.push(...media);
  projectToUpdate.modeling.push(...modeling);

  return await putProjectAPI(projectToUpdate, projectToUpdate.id);
}

export async function removeFileFromProject({ commit, dispatch, state }, { field, fileKey, fileIndex, projectId }) {
  try {
    const key = fileKey.substring(fileKey.lastIndexOf('/')+1);
    const res = await deleteFile(key);
    // remove from selectedProject
    commit('REMOVE_FILE', { field, fileIndex, projectId });
    const project = state.projects.find(item => item.id === projectId)
    dispatch('updateProject')
  } catch (error) {
  }
}