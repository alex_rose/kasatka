export function setRandomProjects(state, projects) {
  state.randomizedProjects = projects;
}

export function setProjects(state, projects) {
  state.projects = projects;
}

export function setTitle(state, title) {
  state.selectedProject.title = title;
}

export function setType(state, type) {
  state.selectedProject.type = type;
}

export function setDescription(state, description) {
  state.selectedProject.description = description;
}

export function setFinishDate(state, finishDate) {
  state.selectedProject.finishDate = finishDate;
}

export function setSquare(state, square) {
  state.selectedProject.square = square;
}

export function setMetaTitle(state, metaTitle) {
  state.selectedProject.meta.title = metaTitle;
}
export function setMetaDescription(state, metaDescription) {
  state.selectedProject.meta.description = metaDescription;
}
export function setMetaImage(state, metaImage) {
  state.selectedProject.meta.image = metaImage;
}
export function setMetaKeywords(state, metaKeywords) {
  state.selectedProject.meta.keywords = metaKeywords;
}
export function setMetaOgType(state, OgType) {
  state.selectedProject.meta.ogType = OgType;
}
export function setMetaOgTitle(state, OgTitle) {
  state.selectedProject.meta.ogTitle = OgTitle;
}
export function setMetaOgUrl(state, OgUrl) {
  state.selectedProject.meta.ogUrl = OgUrl;
}
export function setMetaOgDescription(state, OgDescription) {
  state.selectedProject.meta.ogDescription = OgDescription;
}
export function setMetaOgImage(state, OgImage) {
  state.selectedProject.meta.ogImage = OgImage;
}
export function setLocation(state, location) {
  state.selectedProject.location = location;
}

export const selectProject = (state, project) => {
  state.selectedProject = project;
};

export const setMediaInSelectedProject = (state, media) => {
  state.selectedProject.media = media;
};

export const cleanSelectedProject = (state) => {
  state.selectedProject = {};
};

export const setItem = (state, { type, item }) => {
  state[type] = item;
};

export const REMOVE_FILE = (state, {field, fileIndex, projectId }) => {
  state.selectedProject[field].splice(fileIndex, 1);
  const projectIndex = state.projects.findIndex(item => item.id === projectId);
  state.projects[projectIndex][field].splice(fileIndex, 1);
}