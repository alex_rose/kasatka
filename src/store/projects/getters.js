/*
export function someGetter (state) {
}
*/
export const getRows = (state) => {
  return state.projects;
};

export const getColumns = (state) => {
  return [
    { name: "id", align: "center", label: "id", field: "id", sortable: true },
    {
      name: "title",
      required: true,
      label: "Название проекта",
      align: "left",
      field: (row) => row.title,
      format: (val) => `${val}`,
      sortable: true,
    },
    { name: "created_at", label: "Создано", field: "created_at" },
    {
      name: "finishDate",
      label: "Дата окончания",
      field: "finishDate",
      sortable: true,
    },
    { name: "square", label: "Площадь", field: "square" },
    { name: "action", label: "Действия", field: "action" },
  ];
};

export const getProjects = (state) => {
  return state.projects;
};

export const getProject = (state) => {
  return {...state.selectedProject};
};

export const getChunks = (state) => {
  const media = [...state.selectedProject.media];
  const pointer = Math.floor(media.length / 2);
  const firstChunk = media.slice(0, pointer);
  const secondChunk = media.slice(pointer, media.length);
  return [firstChunk, secondChunk];
};

export const getProjectById = (state) => (id) => {
  return state.projects.find((project) => project.id === id);
};

export function getMeta(state) {
  return {...state.selectedProject.meta};
}


export function getProjectsByBusiness(state) {
  let sorted = [];
  state.projects.forEach((project) => {
    if (project.type === "Бизнес") {
      sorted.push(project);
    }
  });
  return sorted;
}

export function getProjectsByHome(state) {
  let sorted = [];
  state.projects.forEach((project) => {
    if (project.type === "Дома") {
      sorted.push(project);
    }
  });
  return sorted;
}

export function getProjectsByApartaments(state) {
  let sorted = [];
  state.projects.forEach((project) => {
    if (project.type === "Квартиры") {
      sorted.push(project);
    }
  });
  return sorted;
}

export function getAllProjects(state) {
  return state.projects;
}
