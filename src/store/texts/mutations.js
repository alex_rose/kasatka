export function setContacts(state, contacts) {
  state.contacts = contacts;
}

export function setAbout(state, about) {
  state.about = about;
}
