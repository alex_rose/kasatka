export function getContacts(state) {
  const contacts = { ...state.contacts };
  return contacts;
}

export function getAbout(state) {
  const about = { ...state.about };
  return about;
}
