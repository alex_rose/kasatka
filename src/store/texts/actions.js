import {
  fetchAboutAPI,
  putAboutAPI,
  getContactsAPI,
  putContactsAPI,
} from "src/boot/api/texts";
import { Notify } from "quasar";

export async function getAbout({ commit }) {
  const about = await fetchAboutAPI();
  commit("setAbout", about);
  return;
}

export async function getContacts({ commit }) {
  const contacts = await getContactsAPI();
  commit("setContacts", contacts);
  return;
}

export async function updateAbout({ commit }, data) {
  await putAboutAPI(data);
  Notify.create("О нас обновлен");
  return;
}

export async function updateContacts({ commit }, data) {
  await putContactsAPI(data);
  Notify.create("Контакты обновлены");
  return;
}
