import {deleteService, getServices, postService, putService} from "boot/api/service";

export const addService = async ({ commit }, service) => {
  try {
    await postService(service)
  } catch (e) {

  }
}

export async function fetchServices ({commit})  {
  try {
    const data = await getServices()
    commit('SET_LIST', data)
  } catch (e) {

  }
}

export const editService = async ({commit}, service) => {
  try {
    await putService(service)
  } catch (e) {

  }
}
export const delService = async ({commit}, { id, index }) => {
  try {
    await deleteService(id)
    commit('DELETE_SERVICE', index)
  } catch (e) {

  }
}
