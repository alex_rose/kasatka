export function SET_LIST(state, payload) {
  state.list = payload
}

export function ADD_SERVICE(state, payload) {
  state.list.push(payload)
}

export function DELETE_SERVICE(state, index) {
  state.list.splice(index, 1)
}
